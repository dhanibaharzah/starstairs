# starstairs by M Husein ramadhani B

Build a stairs with a star using C

#include <stdio.h>
int main()
{
  int x, y, z;
  printf("n = ");
  scanf("%d", &x);
  for (y = 1; y <= x; y++){
    for(z = 1; z <= y; z++)
      printf("*");
    printf("\n");
  }
  return 0;
}
